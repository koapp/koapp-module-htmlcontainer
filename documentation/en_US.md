# **(How to) Configure the HTML container module**

Among the available modules, this one offers **freedom to introduce static information**.
Although it is really simple to configure, this is how it works.

A wysiwyg editor is available to write as much as you want.
With this module you've be able to create **HTML** files with static info for your app. Simple but effective.
