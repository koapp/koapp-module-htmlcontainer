(function() {
  'use strict';

  angular
    .module('htmlcontainer', [])
    .controller('HtmlContainerController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location'];

  function loadFunction($scope, structureService, $location) {
    //Register upper level modules
    structureService.registerModule($location, $scope, 'htmlcontainer');
  }

}());
